# Diapos de la formation "Forge", PCD NSI Normandie

[Les diapos](https://git-pratique-slides-nsinormandie-f5007accbf083e4a31eaa6654cf4bc.forge.apps.education.fr/)

Ces diapos sont générées avec [Slidev](https://github.com/slidevjs/slidev)!

To start the slide show:

- `npm install`
- `npm run dev`
- visit http://localhost:3030

Edit the [slides.md](./slides.md) to see the changes.

Learn more about Slidev on [documentations](https://sli.dev/).
