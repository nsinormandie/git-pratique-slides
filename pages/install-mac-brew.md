---
layout: section
routeAlias: install-mac-brew
---

# Installation de git

## sous MacOs via HomeBrew

---
layout: default
title: Détails (osx)
---

Détails pour macOS :

- installer [brew (brew.sh/fr/)](https://brew.sh/fr/) si ce n'est pas déjà fait
- puis `brew install git`

---
layout: center
---

<Link to="installation">Retour installations</Link>
