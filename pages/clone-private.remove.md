
---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-01.png"  class="img-fluid w-full">
<figcaption class="text-sm">Naviguer vers le projet sur l'ordi, et éditer le fichier README.md</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-02.png"  class="img-fluid w-full">
<figcaption class="text-sm">Ouvrir avec un éditeur de texte (notepad suffit)    </figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-03.png"  class="img-fluid w-full">
<figcaption class="text-sm">Rajouter une ligne / modifier le fichier</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-04.png"  class="img-fluid w-full">
<figcaption class="text-sm">Enregistrer la modification</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-05.png"  class="img-fluid w-full">
<figcaption class="text-sm">Commande <code>git status</code> pour voir l'état du dépôt</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-06.png"  class="img-fluid w-full">
<figcaption class="text-sm">On prépare l'enregistrement des modifications pour le fichier <code>README.md</code></figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-07.png"  class="img-fluid w-full">
<figcaption class="text-sm">On "enregistre" les modifications avec <code>git commit -m "message"</code>... MAIS erreur car 1ère fois</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-08.png"  class="img-fluid w-full">
<figcaption class="text-sm">On répond à la demande de git de savoir qui on est (nom et email)</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-09.png"  class="img-fluid w-full">
<figcaption class="text-sm">Cette fois le <code>git commit -m "message"</code> fonctionne</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-10.png"  class="img-fluid w-full">
<figcaption class="text-sm">Et on peut envoyer ce lot de modifications sur le serveur</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-11.png"  class="img-fluid w-full">
<figcaption class="text-sm">En rafraîchissant la page on verra les modifications</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-12.png"  class="img-fluid w-full">
<figcaption class="text-sm">Les modifications sont là : fichier, hash du commit, etc...</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-13.png"  class="img-fluid w-full">
<figcaption class="text-sm">Cliquer sur le fichier README.md, et bouton "Modifier"    </figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-14.png"  class="img-fluid w-full">
<figcaption class="text-sm">Modifier le fichier unique</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-15.png"  class="img-fluid w-full">
<figcaption class="text-sm">Rajouter une autre ligne, modifier le fichier</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-16.png"  class="img-fluid w-full">
<figcaption class="text-sm">Editer le message de commit, et valider les modifications    </figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-17.png"  class="img-fluid w-full">
<figcaption class="text-sm">Nouveau commit, avec modifications</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-18.png"  class="img-fluid w-full">
<figcaption class="text-sm">Récupérations de ces modifications avec <code>git pull</code></figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-19.png"  class="img-fluid w-full">
<figcaption class="text-sm">Les modifications sont bien arrivées</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/commit/commit-20.png"  class="img-fluid w-full">
<figcaption class="text-sm"><code>git log</code> nous donne bien l'historique des modifications</figcaption>
</figure>
