---
layout: statement
---

# Avec un IDE

Exemple avec vscode

---
layout: default
---

<figure>
<img src="/screenshots/usages/vscode/vscode-install-01.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/vscode/vscode-install-02.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/vscode/vscode-install-03.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/vscode/vscode-install-04.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/vscode/vscode-install-05.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/vscode/vscode-install-06.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>
