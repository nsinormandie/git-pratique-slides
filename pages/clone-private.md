---
layout: statement
---

# Créer un dépôt

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/clone-private-01.png"  class="img-fluid w-full">
<figcaption class="text-sm">Bouton "Nouveau projet" dans "Projets"</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/clone-private-02.png"  class="img-fluid w-full">
<figcaption class="text-sm">Créer un projet vide</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/clone-private-03.png"  class="img-fluid w-full">
<figcaption class="text-sm">Remplir les informations pour le nouveau projet (nom, ...)</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/clone-private-04.png"  class="img-fluid w-full">
<figcaption class="text-sm">Choisir le groupe dans lequel le dossier sera créé (utilisateur pour l'instant)</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/clone-private-05.png"  class="img-fluid w-full">
<figcaption class="text-sm">Configurer la visibilité (public/privé/interne), création avec le README, et clic "Créer le projet"</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/clone-private-06.png"  class="img-fluid w-full">
<figcaption class="text-sm">Le projet a été crée</figcaption>
</figure>

---
layout: two-cols
---

# Éditer en ligne

- en utilisant l'Ide web de GitLab
- pratique pour un **petit changement rapide** sur une autre machine que la sienne

A éviter pour des modifications plus conséquentes.

::right::

# Éditer en local

- utilisation de git pour synchroniser les modifications
- utilisation de l'IDE de son choix, code testable et débuggable en local

**A privilégier tout le temps sauf quand on n'est pas sur sa machine.**

---
layout: section
---

# Authentification

Il est nécessaire de configurer son identité pour pouvoir envoyer des modifications sur un dépôt distant. Cette opération est à réaliser une seule fois par machine.

---
layout: center
---

# Méthode d'authentification

- **Clé SSH** (Secure Shell) : méthode recommandée, plus sécurisée, compatible 2FA
- ~~**nom d'utilisateur et mot de passe / jeton d'authentification** : moins sécurisée, moins pratique sauf cas particuliers (scripts et automatisations, éventuellement).~~

## Clef ssh

- génération d'une paire de clés, _clef privée_ qui **reste en sécurité** sur la machine, et _clef publique_ à ajouter sur le serveur distant
- authentification via la clef privée (même concept que la signature)

---
layout: default
---

# Déjà une clef SSH ?

Pour vérifier si vous avez déjà une clef SSH, ouvrez un terminal (`Git Bash` sous windows, n'importe quel terminal sous macos et linux) et tapez :

```bash
cd # pour aller dans le répertoire personnel
ls -al .ssh/ # pour lister tous les fichiers du répertoire .ssh
```

- si le répertoire .ssh n'existe pas : vous n'avez pas de clef SSH
- si le répertoire .ssh existe, et contient des fichiers `id_xxxxxx` et `id_xxxxxx.pub` : vous avez déjà une clef SSH, vous pouvez sauter l'étape de génération de clef.

---
layout: default
---

# Création d'une paire de clefs SSH

Dans le terminal, utilisez la commande `ssh-keygen` pour générer une nouvelle paire de clefs SSH :

```bash
ssh-keygen
```

- appuyez sur `Entrée` pour accepter le chemin par défaut
- appuyez sur `Entrée` pour ne pas mettre de mot de passe, ou entrez un mot de passe si vous le souhaitez[^1]

[^1]: Un mot de passe sur la clef SSH est une sécurité supplémentaire, mais peut être un peu plus contraignant à l'usage. Sans mot de passe, la sécurité de la clef est alors celle du poste de travail.

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-config-01.png"  class="img-fluid w-full">
<figcaption class="text-sm">Création du couple de clefs : publique <code>id_ed25519.pub</code> et privée <code>id_ed25519</code></figcaption>
</figure>

---
layout: center
---

# Ajout de la clef publique sur GitLab

Il faut maintenant donner la clef publique à GitLab pour pouvoir s'authentifier.

- naviguez vers votre vers les "Paramètres utilisateur" dans GitLab : `icône utilisateur / préférences / clés SSH`
- copiez le contenu de la clef publique (fichier `id_xxxxx.pub` par défaut)
- cliquez sur "Ajouter une nouvelle clé"
- collez le contenu dans le champ "Clé" et validez

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-config-02.png"  class="img-fluid w-full">
<figcaption class="text-sm">Barre latérale - icône utilisateur</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-config-03.png"  class="img-fluid w-full">
<figcaption class="text-sm">Préférences utilisateur</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-config-04.png"  class="img-fluid w-full">
<figcaption class="text-sm">Clés SSH</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-config-05.png"  class="img-fluid w-full">
<figcaption class="text-sm">Ajouter une nouvelle clé</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-config-06.png"  class="img-fluid w-full">
<figcaption class="text-sm">Afficher le contenu de la clef publique : `cat id_xxxxxx.pub`</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-config-07.png"  class="img-fluid w-full">
<figcaption class="text-sm">Sélectionner la clef, click droit</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-config-08.png"  class="img-fluid w-full">
<figcaption class="text-sm">Copier la clef</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-config-09.png"  class="img-fluid w-full">
<figcaption class="text-sm">Coller la clef dans le champ "Clé", vérifier/modifier les autres champs (la date d'expiration)</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-config-10.png"  class="img-fluid w-full">
<figcaption class="text-sm">La clef est enregistrée</figcaption>
</figure>

---
layout: statement
---

# Clef SSH : ✓

Encore une dernière étape : configurer l'identité locale.

---
title: Identité locale
---

# Configurer son identité

```bash
git config --global user.name "Votre Nom"
git config --global user.email "votre.addresse@email.tld"
```

---
layout: statement
---

# Préparation : ✓

Testons...

---
layout: center
---

# Tester l'authentification

- copier l'URL du dépôt ("Cloner avec SSH")
- `git clone <URL> [<dest>]` dans un terminal
- confirmer l'ajout de l'hôte à la liste des connus (une seule fois, pour chaque hôte)
- si tout se passe bien, le dépôt est cloné

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-clone-01.png"  class="img-fluid w-full">
<figcaption class="text-sm">Copier l'URL de clonage par SSH</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-clone-02.png"  class="img-fluid w-full">
<figcaption class="text-sm"><code>git clone &lt;URL> [&lt;dest>]</code></figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-clone-03.png"  class="img-fluid w-full">
<figcaption class="text-sm">confirmer l'ajout de l'hôte à la liste des connus</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-clone-04.png"  class="img-fluid w-full">
<figcaption class="text-sm">Clonage réussi !</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/ssh-clone-05.png"  class="img-fluid w-full">
<figcaption class="text-sm">Le dossier est bien présent</figcaption>
</figure>

---
layout: statement
---

# Authentification<br> SSH = ✓

Maintenant : utiliser "vraiment" git.

---
layout: center
---

# Tester un cycle de modifications

- modifier un fichier (le `README.md`) dans le dépôt cloné
- `git status` pour voir les modifications
- `git add <fichier>` pour "préparer" l'enregistrement des modifications
- `git commit -m "message"` pour "enregistrer" les modifications
- `git config ...` pour configurer son identité si ce n'est pas déjà fait
- `git push` pour "envoyer" les modifications sur le dépôt distant
- modifier le fichier dans GitLab, enregistrer ces modifications
- `git pull` pour "récupérer" les modifications du dépôt distant
- `git log` pour voir l'historique des modifications
