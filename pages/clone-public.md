---
layout: image-right
image: /screenshots/usages/clone/clone-public-https-01.png
---

# Récupérer un dépôt **public** existant

Naviguer vers le dépôt _public_ de votre choix sur la forge, gitlab ou github, et copier l'URL du dépôt en **https**.

---
layout: image-right
image: /screenshots/usages/clone/clone-public-https-02.png
---

Récupérer l'url en https du dépôt public

Ouvrir un terminal, et taper la commande `git clone` suivi de l'url du dépôt, et optionnellement du nom du dossier de destination.

`git clone <URL> [<dest>]`

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/clone-public-https-03.png"  class="img-fluid w-full">
<figcaption class="text-sm">Ligne de commande, éventuellement précédée de `cd ......`</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/clone-public-https-04.png" class="img-fluid w-full">
<figcaption class="text-sm">Clonage réussi...</figcaption>
</figure>

---
layout: default
---

<figure>
<img src="/screenshots/usages/clone/clone-public-https-05.png" class="img-fluid w-full">
<figcaption class="text-sm">Dossier présent...</figcaption>
</figure>
