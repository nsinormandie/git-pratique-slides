---
theme: seriph
background: https://cover.sli.dev
title: Git en pratique
canvasWidth: 720
info: |
  ## Git en pratique
  Utiliser git au quotidien sans se prendre la tête.
class: text-center
highlighter: shiki
drawings:
  persist: false
transition: fade
mdc: true
---

# Git en pratique

---
title: Historique
---

# Problème : travailler à plusieurs

- versionner son code : qui a fait quoi, quand, pourquoi, comment
- résoudre des conflits : travailler à plusieurs sur le même code, en même temps

---
title: Solutions
---

# Solutions : Au commencement

- moyens "primitifs" : ftp / email / disquette / clef usb
- outils de gestion de version
  - mode de stockage / accès : centralisé / décentralisé
  - mode de gestion des potentiels conflits : fusion / verrouillage

---
title: Premiers outils
---

# Premiers outils

## les ancêtres - centralisés

- cvs : centralisé / fusion
- svn : centralisé / fusion et/ou verrouillage

## Les modernes

- mercurial : décentralisé / fusion - création en 2005,
- **git** : décentralisé / fusion - création en 2005

---
layout: cover
background: https://cover.sli.dev
title: Git
---

# Git

---
title: Histoire
---

# Histoire de git

- 2005 : création par Linus Torvalds
  - de 1991 à 2002 : gestion du noyau Linux par email, patchs et archives
  - 2002 : BitKeeper, logiciel propriétaire, gratuit pour le noyau Linux
  - 2005 : conflit avec BitKeeper ($$$), création de git par Linus Torvalds (en 2 mois)
- 2008 : github
- 2018 : rachat par Microsoft

---
title: Actuel
---

# État actuel

- git est aujourd'hui de-facto standard
  - github, gitlab, bitbucket, etc..
  - intégration continue, déploiement continu, etc..
  - gestion de tickets, wiki, etc..

---
title: Caractéristiques
---

# Caractéristiques

## Décentralisé

- chaque développeur a une copie complète de l'historique
- chaque développeur peut travailler en local
- chaque développeur peut publier ses modifications

## Fusion

- chaque développeur peut travailler en parallèle
- git gère les conflits lors de la fusion du code de plusieurs développeurs

---
title: Fonctionnement
---

# Fonctionnement

Stockage des données = objets :

- blobs (fichiers, nommés par le sha de leur contenu, ou bien empaquetés dans des "packs")
- trees (arborescence)
- commits (instantanés = references vers des trees)
- tags (étiquettes)

---
title: Organisation
---

# Organisation des données

Stocke les données sous forme de "snapshots" (instantanés) de l'ensemble du code via des références / liens entre les objets.

---
title: Etats
---

3 états:

- working directory: le code que vous modifiez
- staging area: les modifications que vous avez préparées pour le prochain commit
- repository: l'historique de tous les commits

---
layout: center
title: Objectifs
---

# Objectifs

- installer git
- configurer git
- utiliser git
- comprendre git
- collaborer, partager

---
title: Histoire
---

---
layout: cover
title: Installation
background: https://cover.sli.dev
---

# Installation

---
layout: center
---

## Déjà installé ?

Tester la version de git (et erreur si non installé) :

- linux : terminal, taper `git --version`
- macos : application "Terminal", taper `git --version`
- windows : rechercher le programme `Git Bash`, si présent l'ouvrir et taper `git --version`

---
layout: default
title: Installation
routeAlias: installation
---

<div class="text-sm">

## Si il faut installer

- sous linux
  - `sudo apt install git` pour debian/ubuntu,
  - `sudo dnf install git` pour fedora,
- sous macOS : fourni avec `XCode`, sinon
  - `brew install git` (avec [brew](https://brew.sh)) - <Link to="install-mac-brew">étapes détaillées</Link>
- sous windows :
  - `choco install git` (avec [chocolatey](https://chocolatey.org/install)) - <Link to="install-win-choco">étapes détaillées</Link>
  - `winget install --id Git.Git -e --source winget` (avec [winget](https://learn.microsoft.com/fr-fr/windows/package-manager/winget/))
  - ou [télécharger sur git-scm.com](https://git-scm.com/download/win) et installer manuellement (voire utiliser la **version _portable_**)

</div>

---
layout: cover
title: Et maintenant ?
background: https://cover.sli.dev
---

# Utilisation "publique"

---
title: Clone public https
src: pages/clone-public.md
---

---
title: Commandes de base
---

# Commandes git

- `git clone <url> [dest]` : cloner un dépôt à partir (de **url** vers **dest**)
- `git log` : consulter l'historique des modifications
- `git pull` : récupérer les modifications distantes

---
layout: cover
title: Utilisation avec identité
background: https://cover.sli.dev
---

# Utilisation "authentifiée"

---
layout: center
title: Objectifs
---

# Objectifs

- ~~créer dépôt personnel, privé~~
- préparer son authentification
- cloner un dépôt privé
- envoyer des modifications sur un dépôt distant

---
title: Utilisation authentifiée
src: pages/clone-private.md
---

---
title: Usage avec un IDE
---

# Git avec un IDE

- [**VSCode**](https://code.visualstudio.com/docs/editor/versioncontrol) : intégré
- [PyCharm / jetbrains](https://www.jetbrains.com/help/idea/set-up-a-git-repository.html) : intégré
- autres IDE : souvent possible, nativement ou via plugins

Très pratique...

---
title: Utilisation vscode
---

# Utilisation de git avec VSCode

- même interface que le Web IDE
- clonage avec vscode ou juste ouverture dossier git
- onglet "source control" pour les commandes de base (commit / push / pull)

---
title: Développement mkdocs en local
layout: cover
background: https://cover.sli.dev
---

# Développement mkdocs en local

---
title: Environnement virtuel
---

# Environnement virtuel

Problème : versions des dépendances différentes selon les projets

Solution : environnement virtuel

---
title: Création d'un environnement virtuel
---

# Création d'un environnement virtuel

```bash
python -m venv .venv
# ou python3 -m venv .venv
# si python ne pointe pas vers python3
```

---
title: Activation de l'environnement virtuel
---

# Activation de l'environnement virtuel

(facultatif)

- sous linux/macOS : `source .venv/bin/activate`
- sous windows : `.venv\Scripts\activate`

---
title: Installation des dépendances
---

# Gestion des dépendances

Fichier `requirements.txt` :

```bash
# Dans l'environnement virtuel
pip install -r requirements.txt
```

---
title: Gestion des dépendances
---

# Gestion des dépendances

- `pip freeze > requirements.txt` : sauvegarder les dépendances complètes (avec version)
- autres outils : `poetry`, `pipenv`, `pip-tools`, `uv`

Le fichier `pyproject.toml` semble être le futur...

---
title: Démarrage de mkdocs
---

# Démarrage de mkdocs

```bash
mkdocs serve
```

---
title: Comprendre git
layout: cover
background: https://cover.sli.dev
---

# Le fonctionnement de git

---
title: Graphe git
---

# Graphe git

```mermaid
flowchart LR
bdb7[commit:bdb7:commit 1]
e965[blob:e965:Hello]
31d7[tree:31d7]
a5ca[tree:a5ca]
cc62[blob:cc62:world]
0a50[tree:0a50]
ce01[blob:ce01:hello]
166e[commit:166e:commit 2]
932f[commit:932f:commit 3]
e624[tree:e624]
7877[blob:7877:issue]

bdb7 --> 31d7
31d7 -- test --> ce01

166e -- parent --> bdb7
166e -- tree --> a5ca
a5ca -- dossier --> 0a50
a5ca -- test --> e965
a5ca -- test2 --> cc62
0a50 -- test3 --> ce01

932f -- parent --> 166e
932f -- tree --> e624
e624 -- test4 --> 7877



HEAD --> refs/heads/issue-1
refs/heads/issue-1 --> 932f
refs/heads/main --> 166e
```

---
title: Collaborer et partager
layout: cover
background: https://cover.sli.dev
---

# Collaborer et partager

---
title: Usage forge
---

# Outil collaboratif

Résolution des conflits : humains...

Outils de type "forge" :

- Communiquer et organiser : tickets
- CI/CD : tester, valider la qualité
- Merge requests : intégrer des modifications

---
title: Conflit..
---

# Création et résolution d'un conflit

- modification d'un fichier `texte` en local, et commit. Contenu : `Hello world`
- modification du même fichier `texte` en ligne, et commit. Contenu : `Bonjour monde`

`git push` : pas possible, `git pull` : conflit à résoudre

Marqueurs de conflit : `<<<<<<<`, `=======`, `>>>>>>>`

Une fois le conflit résolu : `git add <fichier>`, `git commit -m "message"`, `git push`

---
title: Utilisation
---

# Utilisation des tickets et requètes de fusion

- Créer un ticket, discuter
- Cloner le dépôt
- Créer une branche
- Faire des modifications
- Pousser la branche
- Créer une `requètes de fusion`

---
layout: cover
title: Étapes détaillées
background: https://cover.sli.dev
---

# Étapes détaillées

---
layout: default
src: pages/install-win-choco.md
---

---
layout: default
src: pages/install-mac-brew.md
---
